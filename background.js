
// A function to use as callback
function doStuffWithDom(domContent) {
    console.log('I received the following DOM content:\n' + domContent);
};

chrome.browserAction.onClicked.addListener(function (tab) {
    // ...check the URL of the active tab against our pattern and...
  
    // ...if it matches, send a message specifying a callback too
    //chrome.tabs.sendMessage(tab.id, {text: 'report_back'}, doStuffWithDom);
    chrome.tabs.sendMessage(tab.id, {text: 'tokens'}, doStuffWithDom);
    
});